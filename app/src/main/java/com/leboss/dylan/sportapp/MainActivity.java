package com.leboss.dylan.sportapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_main_creation;
    Button btn_main_encours;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_main_creation = (Button)findViewById(R.id.bt_main_creation);
        btn_main_encours = (Button)findViewById(R.id.bt_main_encours);

        btn_main_creation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creationActivite();
            }
        });

        btn_main_encours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                affichageEnCours();
            }
        });


    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this,ActiviteEnCours.class);
        startActivity(intent);
    }

    private void affichageEnCours() {
    }

    private void creationActivite() {
        Intent intent = new Intent(MainActivity.this,CreationActivite.class);
        startActivity(intent);

    }

}
